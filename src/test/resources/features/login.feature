Feature: Log in

  User should be able to log in with existing user.

  @ui @skip
  Scenario: Log in with valid Phone Number and Email
    Given Entered valid phone number and valid email
    And Agreed to License Agreement and Agreed to Marketing information
    When Enters verification code
    And Enters vehicle with Nickname: Nick, License Plate Number: PLATE1 and State: FL - Florida
    And On sign up adds Visa test credit card

  @ui
  Scenario: Test GraphQL Query and Mutation
    Given Request OPT code
    And Verify phone with OTP code
    And Get User account details
    And Set Vehicle