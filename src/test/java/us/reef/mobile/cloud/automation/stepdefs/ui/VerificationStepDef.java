package us.reef.mobile.cloud.automation.stepdefs.ui;

import io.cucumber.java.en.When;
import org.springframework.beans.factory.annotation.Autowired;
import us.reef.mobile.cloud.automation.ui.screens.VerificationScreen;

import static org.assertj.core.api.Assertions.assertThat;

/**
 * @author Nikola Komazec (nikola.komazec@htecgroup.com)
 * Created on: 06/11/2020
 */
public class VerificationStepDef {
    @Autowired
    private VerificationScreen verification;

    @When("^Enter(?:s|ed)? verification code$")
    public void enterVerificationCode() {
        assertThat(verification.isVerificationScreenDisplayed())
                .as("Verification screen is not displayed!")
                .isTrue();
        verification.pressKeyboardTab();
        verification.pressKeyboardTab();
        verification.enterKeyboardNumbers("1234");
        verification.tapOnContinueButton();
    }
}