package us.reef.mobile.cloud.automation.stepdefs.graphql;

import io.cucumber.java.en.Given;
import org.springframework.beans.factory.annotation.Autowired;
import us.reef.mobile.cloud.automation.rest.data.graphql.responses.verification.RequestOtpResponse;
import us.reef.mobile.cloud.automation.rest.data.graphql.responses.verification.VerifyOtpResponse;
import us.reef.mobile.cloud.automation.rest.proxy.graphql.VerificationProxy;

/**
 * @author Nikola Komazec (nikola.komazec@htecgroup.com)
 * Created on: 06/11/2020
 */
public class VerificationGraphQLStepDef {

    @Autowired
    private VerificationProxy verification;

    @Given("^Request OPT code$")
    public void requestOtpCode() {
        final RequestOtpResponse requestOtpResponse = verification.getVerificationOtpCode();
        final String otp = requestOtpResponse.getRequestOtp().getRequestOtpData().getOtp();
    }

    @Given("^Verify phone with OTP code$")
    public void verifyOtp() {
        final VerifyOtpResponse verifyOtpResponse = verification.verifyPhone();
        final String accessToken = verifyOtpResponse.getVerifyOtp().getVerifyOtpData().getAccessToken();
    }
}