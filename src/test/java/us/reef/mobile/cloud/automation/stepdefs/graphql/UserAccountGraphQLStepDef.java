package us.reef.mobile.cloud.automation.stepdefs.graphql;

import io.cucumber.java.en.Given;
import org.springframework.beans.factory.annotation.Autowired;
import us.reef.mobile.cloud.automation.rest.data.graphql.responses.useraccount.UserAccountDetailsResponse;
import us.reef.mobile.cloud.automation.rest.proxy.graphql.UserAccountProxy;

/**
 * @author Nikola Komazec (nikola.komazec@htecgroup.com)
 * Created on: 06/11/2020
 */
public class UserAccountGraphQLStepDef {

    @Autowired
    private UserAccountProxy userAccount;

    @Given("^Get User account details$")
    public void getUserAccountDetails() {
        final UserAccountDetailsResponse userAccountDetails = userAccount.getUserAccountDetails();
    }
}