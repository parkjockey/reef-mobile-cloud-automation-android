package us.reef.mobile.cloud.automation.stepdefs.ui;

import io.cucumber.java.en.Given;
import org.springframework.beans.factory.annotation.Autowired;
import us.reef.mobile.cloud.automation.ui.screens.WelcomeScreen;

import static org.assertj.core.api.Assertions.assertThat;

/**
 * @author Nikola Komazec (nikola.komazec@htecgroup.com)
 * Created on: 06/11/2020
 */
public class WelcomeStepDef {

    @Autowired
    private WelcomeScreen welcome;

    @Given("^Enter(?:s|ed)? ([Vv]alid|[Ii]nvalid) [Pp]hone [Nn]umber and ([Vv]alid|[Ii]nvalid) [Ee]mail$")
    public void enterValidPhoneNumber(final String validPhone, final String validEmail) {
        assertThat(welcome.isWelcomeScreenDisplayed()).as("Welcome screen is not displayed!").isTrue();
        welcome.enterPhoneNumber(validPhone.toLowerCase().equals("valid"));
        welcome.enterEmail(validEmail.toLowerCase().equals("valid"));
    }

    @Given("^([Aa]gree(?:s|d)?|[Dd]isagree(?:s|d)?) to License Agreement and ([Aa]gree(?:s|d)?|[Dd]isagree(?:s|d)?) to Marketing information$")
    public void agreeToLicenseAgreementAndMarketingInformation(final String agreeLicense, final String agreeMarketing) {
        final boolean agreeToLicenseAgreement = agreeLicense.toLowerCase().startsWith("agree");
        final boolean agreeToMarketingInformation = agreeMarketing.toLowerCase().startsWith("agree");
        welcome.agreeToLicenseAgreement(agreeToLicenseAgreement);
        welcome.agreeToMarketingInformation(agreeToMarketingInformation);
        welcome.tapOnContinueButton();
    }
}