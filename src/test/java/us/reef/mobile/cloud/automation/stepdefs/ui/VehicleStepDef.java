package us.reef.mobile.cloud.automation.stepdefs.ui;

import io.cucumber.java.en.Given;
import org.springframework.beans.factory.annotation.Autowired;
import us.reef.mobile.cloud.automation.ui.screens.VehicleScreen;

import static org.assertj.core.api.Assertions.assertThat;

/**
 * @author Nikola Komazec (nikola.komazec@htecgroup.com)
 * Created on: 06/11/2020
 */
public class VehicleStepDef {

    @Autowired
    private VehicleScreen vehicle;

    @Given("^Enter(?:s|ed)? vehicle with [Nn]ickname: (.*), [Ll]icense [Pp]late [Nn]umber: (.*) and [Ss]tate: (.*)$")
    public void fillVehicleDataAndTapOnContinueButton(final String carNickName,
                                                      final String licensePlateNumber,
                                                      final String licensePlateState) {
        assertThat(vehicle.isVehicleScreenDisplayed()).as("Vehicle screen is not displayed!").isTrue();
        vehicle.fillInVehicleForm(carNickName, licensePlateNumber, licensePlateState);
        vehicle.tapOnSaveButton();
    }
}