package us.reef.mobile.cloud.automation.stepdefs.ui;

import io.cucumber.java.en.When;
import org.springframework.beans.factory.annotation.Autowired;
import us.reef.mobile.cloud.automation.ui.screens.CreditCardScreen;

import static org.assertj.core.api.Assertions.assertThat;

/**
 * @author Nikola Komazec (nikola.komazec@htecgroup.com)
 * Created on: 30/11/2020
 */
public class CreditCardStepDef {

    @Autowired
    private CreditCardScreen creditCard;

    @When("^On (?:[Ss]ign [Uu]p|[Ss]ign [Ii]n)? [Aa]dd(?:ed|s)? ([Vv]isa|[Mm]aster [Cc]ard|[Aa]merican [Ee]xpress|[Dd]iscover) test credit card$")
    public void addTestCreditCard(final String cardProvider) throws InterruptedException {
        assertThat(creditCard.isCreditCardScreenDisplayed())
                .as("Credit card screen is not displayed!").isTrue();
        creditCard.addTestCreditCard(cardProvider);
    }
}
