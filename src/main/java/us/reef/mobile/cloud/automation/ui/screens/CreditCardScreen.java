package us.reef.mobile.cloud.automation.ui.screens;

import lombok.extern.slf4j.Slf4j;
import org.openqa.selenium.By;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;
import us.reef.mobile.cloud.automation.rest.data.creditcard.CreditCard;
import us.reef.mobile.cloud.automation.storage.CreditCardEntity;
import us.reef.mobile.cloud.automation.ui.base.BaseDriver;
import us.reef.mobile.cloud.automation.ui.base.BasePage;

import static io.cucumber.spring.CucumberTestContext.SCOPE_CUCUMBER_GLUE;

/**
 * @author Nikola Komazec (nikola.komazec@htecgroup.com)
 * Created on: 30/11/2020
 */
@Slf4j
@Component
@Scope(SCOPE_CUCUMBER_GLUE)
public class CreditCardScreen extends BasePage {

    final By creditCardPageId = By.xpath("//*[@resource-id='com.reeftechnology.reef.mobile:id/wv_add_payment']");
    final By creditCardNumberField = By.xpath("//*[@resource-id='x_card_num']");
    final By fullCardNameField = By.xpath("//*[@resource-id='exact_cardholder_name']");
    final By cvvField = By.xpath("//*[@resource-id='x_card_code']");
    final By monthSelection = By.xpath("//*[@resource-id='expire_mm']");
    final By yearSelection = By.xpath("//*[@resource-id='expire_yy']");
    final By addCreditCardButton = By.xpath("//*[@class='android.widget.Button']");

    public CreditCardScreen(final BaseDriver baseDriver) {
        super(baseDriver);
    }

    /**
     * Check is credit card screen displayed.
     *
     * @return true if it is displayed, otherwise false.
     */
    public boolean isCreditCardScreenDisplayed() {
        log.info("Checking is Credit Card screen displayed.");
        return waitForElementToBeDisplayed(creditCardPageId);
    }

    /**
     * Add test credit card by a specific card provider.
     *
     * @param cardProvider Card Provider (Visa, Master Card, American Express, Discover)
     */
    public void addTestCreditCard(final String cardProvider) {
        final CreditCard creditCardNumber;
        final String fullCardName = "Automation Test";
        final String cvvNumber = "123";
        creditCardNumber = CreditCard.getCreditCardNumber(cardProvider.toUpperCase());
        waitAndSendKeys(creditCardNumberField, creditCardNumber.card);
        log.info("Entered credit card number: " + creditCardNumber.card);
        waitAndSendKeys(fullCardNameField, fullCardName);
        log.info("Entered card name: " + fullCardName);
        waitAndSendKeys(cvvField, cvvNumber);
        log.info("Entered CVV number: " + cvvNumber);
        waitAndTap(monthSelection);
        waitAndTap(By.xpath("//*[@text='01']"));
        log.info("Selected month: 01");
        waitAndTap(yearSelection);
        waitAndTap(By.xpath("//*[@text='2022']"));
        log.info("Selected year: 2022");
        waitAndTap(addCreditCardButton);
        log.info("Clicked Add Credit Card button.");
        final CreditCardEntity card = CreditCardEntity.builder()
                .cardNumber(creditCardNumber.card)
                .cardName(fullCardName)
                .cvv(cvvNumber)
                .build();
        getStorage().getCreditCards().add(card);
    }
}