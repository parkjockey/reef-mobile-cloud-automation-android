package us.reef.mobile.cloud.automation.rest.data.graphql.variables.common;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import lombok.Setter;

/**
 * @author Nikola Komazec (nikola.komazec@htecgroup.com)
 * Created on: 06/11/2020
 */
@Setter
public class AppVersionVariables {
    @SerializedName("majorVersion")
    @Expose
    private Integer majorVersion = 2;
    @SerializedName("minorVersion")
    @Expose
    private Integer minorVersion = 1;
    @SerializedName("appType")
    @Expose
    private String appType = "type";
    @SerializedName("appUTCOffset")
    @Expose
    private Integer appUTCOffset = -2;
    @SerializedName("languageCode")
    @Expose
    private String languageCode = "en";
    @SerializedName("operatingSystem")
    @Expose
    private String operatingSystem = "android";
}