package us.reef.mobile.cloud.automation.rest.data.graphql.responses.useraccount;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import us.reef.mobile.cloud.automation.rest.data.graphql.responses.common.CreditCard;
import us.reef.mobile.cloud.automation.rest.data.graphql.responses.common.Phone;
import us.reef.mobile.cloud.automation.rest.data.graphql.responses.common.ResponseStatus;
import us.reef.mobile.cloud.automation.rest.data.graphql.responses.common.Vehicle;

import java.util.List;

/**
 * @author Nikola Komazec (nikola.komazec@htecgroup.com)
 * Created on: 06/11/2020
 */
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class UserAccountDetailsResponse {
    @JsonProperty("data")
    private UserAccountDetails userAccountDetails;

    @Getter
    @Setter
    @NoArgsConstructor
    @AllArgsConstructor
    public static class UserAccountDetails {
        @JsonProperty("getUserAccountDetails")
        private UserAccountData userAccountData;
    }

    @Getter
    @Setter
    @NoArgsConstructor
    @AllArgsConstructor
    public static class UserAccountData {
        private String emailAddress;
        private Phone phone;
        private List<Vehicle> vehicles;
        private List<CreditCard> creditCards;
        private ResponseStatus responseStatus;
    }
}
