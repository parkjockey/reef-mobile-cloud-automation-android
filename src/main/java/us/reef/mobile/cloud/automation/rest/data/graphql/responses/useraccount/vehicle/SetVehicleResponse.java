package us.reef.mobile.cloud.automation.rest.data.graphql.responses.useraccount.vehicle;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import us.reef.mobile.cloud.automation.rest.data.graphql.responses.common.ResponseStatus;
import us.reef.mobile.cloud.automation.rest.data.graphql.responses.common.Vehicle;

import java.util.List;

/**
 * @author Nikola Komazec (nikola.komazec@htecgroup.com)
 * Created on: 09/11/2020
 */
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class SetVehicleResponse {
    @JsonProperty("data")
    private SetVehicle setVehicle;

    @Getter
    @Setter
    @NoArgsConstructor
    @AllArgsConstructor
    public static class SetVehicle {
        @JsonProperty("setVehicle")
        private SetVehicleData setVehicleData;
    }

    @Getter
    @Setter
    @NoArgsConstructor
    @AllArgsConstructor
    public static class SetVehicleData {
        private List<Vehicle> vehicles;
        private ResponseStatus responseStatus;
    }
}