package us.reef.mobile.cloud.automation.utils;

import lombok.experimental.UtilityClass;

import java.util.Random;

/**
 * @author Nikola Komazec (nikola.komazec@htecgroup.com)
 * Created on: 06/11/2020
 */
@UtilityClass
public class TextUtil {

    /**
     * Get valid email
     *
     * @return valid email
     */
    public String getValidEmail() {
        return "reef.mobile.automation+" + new Random().nextInt(99999999) + "@gmail.com";
    }


    /**
     * Get invalid email
     *
     * @return invalid email
     */
    public String getInvalidEmail() {
        return "Mobile-Automation-" + new Random().nextInt(99999999) + "@.com";
    }
}
