package us.reef.mobile.cloud.automation.utils;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.google.gson.Gson;
import lombok.experimental.UtilityClass;
import us.reef.mobile.cloud.automation.exceptions.MobileAutomationException;

import java.io.*;

/**
 * @author Nikola Komazec (nikola.komazec@htecgroup.com)
 * Created on: 06/11/2020
 */
@UtilityClass
public class GraphQLUtil {

    /**
     * Parses the given graphql file object to the string suitable for the request
     * payload.
     *
     * @param file      - A {@link File} object
     * @param variables - The variables in the form of {@link ObjectNode}
     * @return A string suitable for the request payload.
     */
    public static String parseGraphql(final File file, final Object variables) {
        final Gson gson = new Gson();
        try {
            final String graphqlFileContent = convertInputStreamToString(new FileInputStream(file));
            return convertToGraphqlString(graphqlFileContent, gson.toJson(variables));
        } catch (final Exception e) {
            throw new MobileAutomationException("Could not find graphQL file!" +
                    "Error: {}", e.toString());
        }
    }

    private static String convertInputStreamToString(final InputStream inputStream) throws IOException {
        final StringBuilder sb = new StringBuilder();
        try (final BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream))) {
            String line;
            while ((line = bufferedReader.readLine()) != null) {
                sb.append(line).append("\n");
            }
        }
        return sb.toString();
    }

    private static String convertToGraphqlString(final String graphql, final String variables) throws JsonProcessingException {
        final ObjectMapper oMapper = new ObjectMapper();
        final ObjectNode oNode = oMapper.createObjectNode();
        try {
            oNode.put("query", graphql);
            oNode.put("variables", variables);
        } catch (final NullPointerException nullPointer) {
            oNode.put("query", graphql);
        }
        return oMapper.writeValueAsString(oNode);
    }
}