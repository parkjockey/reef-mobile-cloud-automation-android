package us.reef.mobile.cloud.automation.storage;

import lombok.Getter;
import lombok.Setter;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;
import us.reef.mobile.cloud.automation.exceptions.MobileAutomationException;
import us.reef.mobile.cloud.automation.storage.scenario.ScenarioEntity;
import us.reef.mobile.cloud.automation.storage.testrail.TestRailEntity;

import java.util.ArrayList;
import java.util.List;

import static io.cucumber.spring.CucumberTestContext.SCOPE_CUCUMBER_GLUE;

/**
 * @author Nikola Komazec (nikola.komazec@htecgroup.com)
 * Created on: 06/11/2020
 */
@Setter
@Getter
@Component
@Scope(SCOPE_CUCUMBER_GLUE)
public class Storage {

    private ScenarioEntity scenario = new ScenarioEntity();
    private TestRailEntity testRail = new TestRailEntity();

    private UserEntity user = new UserEntity();
    private List<VehicleEntity> vehicles = new ArrayList<>();
    private List<CreditCardEntity> creditCards = new ArrayList<>();

    /**
     * Get last Vehicle from Storage.
     *
     * @return Last Vehicle from storage {@link VehicleEntity}
     */
    public VehicleEntity getLastVehicle() {
        return getVehicles().stream().reduce((first, last) -> last)
                .orElseThrow(() -> new MobileAutomationException("There is no last vehicle in storage."));
    }

    /**
     * Get last Credit Card from Storage.
     *
     * @return Last Credit Card from storage {@link CreditCardEntity}
     */
    public CreditCardEntity getLastCreditCard() {
        return getCreditCards().stream().reduce((first, last) -> last)
                .orElseThrow(() -> new MobileAutomationException("There is no last credit card in storage."));
    }
}