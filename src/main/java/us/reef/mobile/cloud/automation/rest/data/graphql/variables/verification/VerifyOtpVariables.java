package us.reef.mobile.cloud.automation.rest.data.graphql.variables.verification;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import lombok.Setter;
import us.reef.mobile.cloud.automation.rest.data.graphql.variables.common.AppVersionVariables;
import us.reef.mobile.cloud.automation.rest.data.graphql.variables.common.PhoneVariables;

/**
 * @author Nikola Komazec (nikola.komazec@htecgroup.com)
 * Created on: 09/11/2020
 */
@Setter
public class VerifyOtpVariables {
    @SerializedName("verifyOtpInput")
    @Expose
    private VerifyOtpInputDataVariables verifyOtpInputDataVariables;

    @Setter
    public static class VerifyOtpInputDataVariables {
        @SerializedName("emailAddress")
        @Expose
        private String emailAddress;
        @SerializedName("phone")
        @Expose
        private PhoneVariables phone;
        @SerializedName("otp")
        @Expose
        private String opt;
        @SerializedName("appVersion")
        @Expose
        private AppVersionVariables appVersion;
    }
}
