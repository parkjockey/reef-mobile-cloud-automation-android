package us.reef.mobile.cloud.automation.utils;

import io.appium.java_client.android.nativekey.AndroidKey;
import lombok.experimental.UtilityClass;
import us.reef.mobile.cloud.automation.exceptions.MobileAutomationException;

/**
 * @author Nikola Komazec (nikola.komazec@htecgroup.com)
 * Created on: 06/11/2020
 */
@UtilityClass
public class KeyboardUtil {

    /**
     * Get Android Key.
     *
     * @param number number which should be returned as Android Key
     * @return {@link AndroidKey}
     */
    public AndroidKey getAndroidKey(final char number) {
        switch (number) {
            case '0':
                return AndroidKey.DIGIT_0;
            case '1':
                return AndroidKey.DIGIT_1;
            case '2':
                return AndroidKey.DIGIT_2;
            case '3':
                return AndroidKey.DIGIT_3;
            case '4':
                return AndroidKey.DIGIT_4;
            case '5':
                return AndroidKey.DIGIT_5;
            case '6':
                return AndroidKey.DIGIT_6;
            case '7':
                return AndroidKey.DIGIT_7;
            case '8':
                return AndroidKey.DIGIT_8;
            case '9':
                return AndroidKey.DIGIT_9;
            default:
                throw new MobileAutomationException("Number invalid {}.", number);
        }
    }
}
