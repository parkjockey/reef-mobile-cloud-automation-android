package us.reef.mobile.cloud.automation.rest.data.graphql.responses.verification;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import us.reef.mobile.cloud.automation.rest.data.graphql.responses.common.Phone;
import us.reef.mobile.cloud.automation.rest.data.graphql.responses.common.ResponseStatus;

/**
 * @author Nikola Komazec (nikola.komazec@htecgroup.com)
 * Created on: 06/11/2020
 */
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class RequestOtpResponse {
    @JsonProperty("data")
    private RequestOtp requestOtp;

    @Getter
    @Setter
    @NoArgsConstructor
    @AllArgsConstructor
    public static class RequestOtp {
        @JsonProperty("requestOTP")
        private RequestOtpData requestOtpData;
    }

    @Getter
    @Setter
    @NoArgsConstructor
    @AllArgsConstructor
    public static class RequestOtpData {
        private String emailAddress;
        private Phone phone;
        private String otp;
        private ResponseStatus responseStatus;
    }
}