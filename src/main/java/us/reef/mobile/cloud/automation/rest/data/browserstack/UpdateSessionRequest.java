package us.reef.mobile.cloud.automation.rest.data.browserstack;

import lombok.*;
import org.springframework.lang.Nullable;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class UpdateSessionRequest {
    private String status;
    @Nullable
    private String reason;
}
