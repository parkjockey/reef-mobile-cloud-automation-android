package us.reef.mobile.cloud.automation.storage.scenario;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * @author Nikola Komazec (nikola.komazec@htecgroup.com)
 * Created on: 06/11/2020
 */
@Getter
@Setter
@NoArgsConstructor
public class ScenarioEntity {
    private String scenarioName;
}
