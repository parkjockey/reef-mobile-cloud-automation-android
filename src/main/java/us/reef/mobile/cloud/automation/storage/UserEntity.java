package us.reef.mobile.cloud.automation.storage;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * @author Nikola Komazec (nikola.komazec@htecgroup.com)
 * Created on: 06/11/2020
 */
@Getter
@Setter
@NoArgsConstructor
public class UserEntity {
    private String phoneNumber;
    private String email;
}
