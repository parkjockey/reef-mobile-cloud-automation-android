package us.reef.mobile.cloud.automation.rest.proxy;

import com.browserstack.appautomate.AppAutomateClient;
import com.browserstack.automate.Automate;
import com.browserstack.client.BrowserStackClient;
import com.browserstack.client.exception.BrowserStackException;
import io.cucumber.java.Scenario;
import io.restassured.response.Response;
import lombok.extern.slf4j.Slf4j;
import org.apache.http.HttpStatus;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;
import us.reef.mobile.cloud.automation.exceptions.MobileAutomationException;
import us.reef.mobile.cloud.automation.rest.client.BaseRestClient;
import us.reef.mobile.cloud.automation.rest.data.browserstack.UpdateSessionRequest;

import static io.cucumber.spring.CucumberTestContext.SCOPE_CUCUMBER_GLUE;

/**
 * @author Nikola Komazec (nikola.komazec@htecgroup.com)
 * Created on: 06/11/2020
 */
@Slf4j
@Component
@Scope(SCOPE_CUCUMBER_GLUE)
public class BrowserStackProxy {

    @Value("${bs.url}")
    private String url;

    @Value("${bs.username}")
    private String username;

    @Value("${bs.password}")
    private String password;

    /**
     * Set Test Scenario status in BrowserStack.
     *
     * @param scenario scenario for which we update its status.
     */
    public void setTestScenarioStatus(final Scenario scenario) {

        final BrowserStackClient appAutomateClient = new AppAutomateClient(username, password);
        try {
            final String buildId = appAutomateClient.getBuilds(Automate.BuildStatus.RUNNING).get(0).getId();
            log.info("Found buildId {}", buildId);
            final String sessionId = appAutomateClient.getSessions(buildId, Automate.BuildStatus.RUNNING).get(0).getId();
            log.info("Found sessionId {}", sessionId);
            final BaseRestClient baseRestClient = new BaseRestClient(url);
            final UpdateSessionRequest updateSessionRequest = UpdateSessionRequest.builder()
                    .status(scenario.getStatus().name().toLowerCase())
                    .reason(null)
                    .build();

            log.info("Updating session status with status {}", scenario.getStatus().name().toLowerCase());
            final Response updateSession = baseRestClient
                    .putStatusOnBrowserStack(updateSessionRequest, "sessions/" + sessionId + ".json", username, password);

            if (updateSession.getStatusCode() != HttpStatus.SC_OK) {
                throw new MobileAutomationException("Session not updated! Expected status {} but was {}, Body: {}",
                        HttpStatus.SC_OK, updateSession.getStatusCode(), updateSession.getBody().prettyPrint());
            }
        } catch (final BrowserStackException e) {
            throw new MobileAutomationException("Error when updating session status: {}", e.toString());
        }
    }
}