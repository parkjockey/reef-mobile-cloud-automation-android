package us.reef.mobile.cloud.automation.storage.testrail;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.List;

/**
 * @author Nikola Komazec (nikola.komazec@htecgroup.com)
 * Created on: 06/11/2020
 */
@Getter
@Setter
@NoArgsConstructor
public class TestRailEntity {
    private List<Integer> caseIds;
}
