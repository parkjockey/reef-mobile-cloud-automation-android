package us.reef.mobile.cloud.automation.rest.data.graphql.responses.common;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;

/**
 * @author Nikola Komazec (nikola.komazec@htecgroup.com)
 * Created on: 06/11/2020
 */
@Getter
@NoArgsConstructor
@AllArgsConstructor
public class Phone {
    private String mobilePhoneCountryCode;
    private String mobilePhoneNumber;
    private String normalizedPhoneNumber;
}
