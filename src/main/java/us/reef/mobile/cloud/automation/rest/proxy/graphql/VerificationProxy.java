package us.reef.mobile.cloud.automation.rest.proxy.graphql;

import com.google.api.client.http.HttpStatusCodes;
import io.restassured.response.Response;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;
import us.reef.mobile.cloud.automation.exceptions.MobileAutomationException;
import us.reef.mobile.cloud.automation.rest.client.BaseRestClient;
import us.reef.mobile.cloud.automation.rest.data.graphql.responses.verification.RequestOtpResponse;
import us.reef.mobile.cloud.automation.rest.data.graphql.responses.verification.VerifyOtpResponse;
import us.reef.mobile.cloud.automation.rest.data.graphql.variables.common.AppVersionVariables;
import us.reef.mobile.cloud.automation.rest.data.graphql.variables.common.PhoneVariables;
import us.reef.mobile.cloud.automation.rest.data.graphql.variables.verification.RequestOtpVariables;
import us.reef.mobile.cloud.automation.rest.data.graphql.variables.verification.VerifyOtpVariables;
import us.reef.mobile.cloud.automation.utils.GraphQLUtil;

import java.io.File;

import static io.cucumber.spring.CucumberTestContext.SCOPE_CUCUMBER_GLUE;

/**
 * @author Nikola Komazec (nikola.komazec@htecgroup.com)
 * Created on: 06/11/2020
 */
@Slf4j
@Component
@Scope(SCOPE_CUCUMBER_GLUE)
public class VerificationProxy {

    static File requestOtpFile = new File("src/test/resources/graphql/verification/requestOTP.graphql");
    static File verifyOtpFile = new File("src/test/resources/graphql/verification/verifyOTP.graphql");
    @Value("${reef-mobile-graphql.url}")
    public String graphqlUrl;

    /**
     * Get Verification OTP Code.
     *
     * @return verification OTP response {@link RequestOtpResponse}
     */
    public RequestOtpResponse getVerificationOtpCode() {
        final BaseRestClient baseRestClient = new BaseRestClient(graphqlUrl);
        final PhoneVariables phoneVariables = new PhoneVariables();
        phoneVariables.setMobilePhoneCountryCode("1");
        phoneVariables.setMobilePhoneNumber("(404) 444-4444");
        final RequestOtpVariables.RequestOtpInputDataVariables requestOTPInputDataVariables = new RequestOtpVariables.RequestOtpInputDataVariables();
        requestOTPInputDataVariables.setEmailAddress("x@x.com");
        requestOTPInputDataVariables.setPhone(phoneVariables);
        requestOTPInputDataVariables.setAppVersion(new AppVersionVariables());
        requestOTPInputDataVariables.setIsOTPInResp(true);
        final RequestOtpVariables requestOTPVariables = new RequestOtpVariables();
        requestOTPVariables.setRequestOtpInputDataVariables(requestOTPInputDataVariables);

        final Response response;
        final String graphqlPayload = GraphQLUtil.parseGraphql(requestOtpFile, requestOTPVariables);
        response = baseRestClient.post(graphqlPayload, "/graphql");
        if (response.getStatusCode() != HttpStatusCodes.STATUS_CODE_OK) {
            throw new MobileAutomationException("Get request otp response status code was {}, expected {}!",
                    response.getStatusCode(), HttpStatusCodes.STATUS_CODE_OK);
        }
        return response.as(RequestOtpResponse.class);
    }

    /**
     * Verify phone with OTP code.
     *
     * @return verification phone response {@link VerifyOtpResponse}
     */
    public VerifyOtpResponse verifyPhone() {
        final BaseRestClient baseRestClient = new BaseRestClient(graphqlUrl);
        final PhoneVariables phoneVariables = new PhoneVariables();
        phoneVariables.setMobilePhoneCountryCode("1");
        phoneVariables.setMobilePhoneNumber("(404) 444-4444");
        final VerifyOtpVariables.VerifyOtpInputDataVariables verifyOtpInputDataVariables = new VerifyOtpVariables.VerifyOtpInputDataVariables();
        verifyOtpInputDataVariables.setAppVersion(new AppVersionVariables());
        verifyOtpInputDataVariables.setEmailAddress("x@x.com");
        verifyOtpInputDataVariables.setOpt("1234");
        verifyOtpInputDataVariables.setPhone(phoneVariables);
        final VerifyOtpVariables verifyOtpVariables = new VerifyOtpVariables();
        verifyOtpVariables.setVerifyOtpInputDataVariables(verifyOtpInputDataVariables);

        final Response response;
        final String graphqlPayload = GraphQLUtil.parseGraphql(verifyOtpFile, verifyOtpVariables);
        response = baseRestClient.post(graphqlPayload, "/graphql");
        if (response.getStatusCode() != HttpStatusCodes.STATUS_CODE_OK) {
            throw new MobileAutomationException("Verify opt response status code was {}, expected {}!",
                    response.getStatusCode(), HttpStatusCodes.STATUS_CODE_OK);
        }
        return response.as(VerifyOtpResponse.class);
    }
}