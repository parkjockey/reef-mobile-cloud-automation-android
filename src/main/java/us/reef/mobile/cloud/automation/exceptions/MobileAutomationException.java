package us.reef.mobile.cloud.automation.exceptions;

import org.slf4j.helpers.MessageFormatter;

public class MobileAutomationException extends RuntimeException {

    /**
     * Mobile Automation Exception with specified message.
     *
     * @param pattern   some information regarding why this exception is thrown
     * @param arguments arguments which we want to shown in the exception
     */
    public MobileAutomationException(final String pattern, final Object... arguments) {
        super(MessageFormatter.arrayFormat(pattern, arguments).getMessage());
    }

    /**
     * Mobile Automation Exception with specified exception
     *
     * @param exception exception which will be re-thrown
     */
    public MobileAutomationException(final Exception exception) {
        super(exception);
    }
}
