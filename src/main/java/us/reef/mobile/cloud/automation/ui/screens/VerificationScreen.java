package us.reef.mobile.cloud.automation.ui.screens;

import lombok.extern.slf4j.Slf4j;
import org.openqa.selenium.By;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;
import us.reef.mobile.cloud.automation.exceptions.MobileAutomationException;
import us.reef.mobile.cloud.automation.ui.base.BaseDriver;
import us.reef.mobile.cloud.automation.ui.base.BasePage;

import static io.cucumber.spring.CucumberTestContext.SCOPE_CUCUMBER_GLUE;

/**
 * @author Nikola Komazec (nikola.komazec@htecgroup.com)
 * Created on: 06/11/2020
 */
@Slf4j
@Component
@Scope(SCOPE_CUCUMBER_GLUE)
public class VerificationScreen extends BasePage {

    final By verificationScreenId = By.id("com.reeftechnology.reef.mobile:id/title");
    final By continueButton = By.id("com.reeftechnology.reef.mobile:id/continue_button");

    public VerificationScreen(final BaseDriver baseDriver) {
        super(baseDriver);
    }

    /**
     * Check is Verification screen displayed by validating that "Enter verification code" text is displayed.
     *
     * @return true if it is displayed, otherwise false.
     */
    public boolean isVerificationScreenDisplayed() {
        log.info("Checking is Verification screen displayed.");
        return waitForElementToBeDisplayed(verificationScreenId);
    }

    /**
     * Tap on Continue button.
     */
    public void tapOnContinueButton() {
        if (driver.findElement(continueButton).getAttribute("enabled").equals("true")) {
            waitAndTap(continueButton);
            log.info("Tapped on Continue button.");
        } else {
            throw new MobileAutomationException("Continue button is not enabled!");
        }
    }
}