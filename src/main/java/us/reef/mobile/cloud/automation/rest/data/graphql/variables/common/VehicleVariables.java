package us.reef.mobile.cloud.automation.rest.data.graphql.variables.common;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import lombok.Setter;

/**
 * @author Nikola Komazec (nikola.komazec@htecgroup.com)
 * Created on: 09/11/2020
 */
@Setter
public class VehicleVariables {
    @SerializedName("carNickName")
    @Expose
    private String carNickName;
    @SerializedName("licensePlateNumber")
    @Expose
    private String licensePlateNumber;
    @SerializedName("licensePlateState")
    @Expose
    private String licensePlateState;
    @SerializedName("isOversized")
    @Expose
    private Boolean isOversized;
    @SerializedName("isPrimary")
    @Expose
    private Boolean isPrimary;
}