package us.reef.mobile.cloud.automation.rest.proxy.graphql;

import com.google.api.client.http.HttpStatusCodes;
import io.restassured.response.Response;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;
import us.reef.mobile.cloud.automation.exceptions.MobileAutomationException;
import us.reef.mobile.cloud.automation.rest.client.BaseRestClient;
import us.reef.mobile.cloud.automation.rest.data.graphql.responses.useraccount.vehicle.SetVehicleResponse;
import us.reef.mobile.cloud.automation.rest.data.graphql.variables.common.AppVersionVariables;
import us.reef.mobile.cloud.automation.rest.data.graphql.variables.common.VehicleVariables;
import us.reef.mobile.cloud.automation.rest.data.graphql.variables.vehicle.SetVehicleVariables;
import us.reef.mobile.cloud.automation.utils.GraphQLUtil;

import java.io.File;

import static io.cucumber.spring.CucumberTestContext.SCOPE_CUCUMBER_GLUE;

/**
 * @author Nikola Komazec (nikola.komazec@htecgroup.com)
 * Created on: 09/11/2020
 */
@Slf4j
@Component
@Scope(SCOPE_CUCUMBER_GLUE)
public class VehicleProxy {

    static File setVehicleFile = new File("src/test/resources/graphql/account/vehicle/setVehicle.graphql");
    @Value("${reef-mobile-graphql.url}")
    public String graphqlUrl;

    /**
     * Set new vehicle.
     *
     * @return Added vehicle response {@link SetVehicleResponse}
     */
    public SetVehicleResponse setVehicle() {
        final BaseRestClient baseRestClient = new BaseRestClient(graphqlUrl);
        final VehicleVariables vehicleVariables = new VehicleVariables();
        vehicleVariables.setCarNickName("Nick");
        vehicleVariables.setLicensePlateNumber("PLATE1");
        vehicleVariables.setLicensePlateState("FL - Florida");
        vehicleVariables.setIsOversized(false);
        vehicleVariables.setIsPrimary(true);
        final SetVehicleVariables.SetVehicleInputDataVariables setVehicleInputDataVariables = new SetVehicleVariables.SetVehicleInputDataVariables();
        setVehicleInputDataVariables.setAction(0);
        setVehicleInputDataVariables.setAppVersionVariables(new AppVersionVariables());
        setVehicleInputDataVariables.setVehicleVariables(vehicleVariables);
        final SetVehicleVariables setVehicleVariables = new SetVehicleVariables();
        setVehicleVariables.setSetVehicleInputDataVariables(setVehicleInputDataVariables);

        final Response response;
        final String graphqlPayload = GraphQLUtil.parseGraphql(setVehicleFile, setVehicleVariables);
        response = baseRestClient.post(graphqlPayload, "/graphql");
        if (response.getStatusCode() != HttpStatusCodes.STATUS_CODE_OK) {
            throw new MobileAutomationException("Set vehicle response status code was {}, expected {}!",
                    response.getStatusCode(), HttpStatusCodes.STATUS_CODE_OK);
        }
        return response.as(SetVehicleResponse.class);
    }

}