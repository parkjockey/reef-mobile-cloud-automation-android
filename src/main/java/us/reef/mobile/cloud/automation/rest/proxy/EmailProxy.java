package us.reef.mobile.cloud.automation.rest.proxy;

import lombok.extern.slf4j.Slf4j;
import org.awaitility.Awaitility;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;
import us.reef.mobile.cloud.automation.exceptions.MobileAutomationException;
import us.reef.mobile.cloud.automation.storage.Storage;

import javax.mail.*;
import javax.mail.internet.MimeMultipart;
import javax.mail.search.AndTerm;
import javax.mail.search.BodyTerm;
import javax.mail.search.SearchTerm;
import javax.mail.search.SubjectTerm;
import java.io.IOException;
import java.util.Properties;
import java.util.concurrent.TimeUnit;

import static io.cucumber.spring.CucumberTestContext.SCOPE_CUCUMBER_GLUE;

/**
 * @author Nikola Komazec (nikola.komazec@htecgroup.com)
 * Created on: 17/11/2020
 */
@Slf4j
@Component
@Scope(SCOPE_CUCUMBER_GLUE)
public class EmailProxy {

    Message[] messages = new Message[0];
    Message message;
    String emailSubject;
    String emailRecipient;
    @Autowired
    private Storage storage;
    @Value("${gmail.username}")
    private String username;
    @Value("${gmail.password}")
    private String password;

    /**
     * Check is email with specific subject received and present in recipients inbox folder
     *
     * @param emailSubject   subject of an email
     * @param emailRecipient email recipient
     * @return true if email with subject is present in recipients inbox folder, otherwise false
     */
    public boolean isMailReceivedByReceiver(final String emailSubject, final String emailRecipient) {
        try {
            connectToGmailAndSearchForEmail(emailSubject);
            for (final Message inboxMessage : messages) {
                final Address[] addresses = inboxMessage.getRecipients(Message.RecipientType.TO);
                for (final Address address : addresses) {
                    if (inboxMessage.getSubject().contains(emailSubject) &&
                            address.toString().equals(emailRecipient)) {
                        log.info("Found the Email with Subject: {} for Recipient: {}.", emailSubject, emailRecipient);
                        this.emailSubject = emailSubject;
                        this.emailRecipient = emailRecipient;
                        this.message = inboxMessage;
                        return true;
                    }
                }
            }
        } catch (final MessagingException e) {
            throw new MobileAutomationException(e.toString());
        }
        Awaitility.await()
                .atMost(60, TimeUnit.SECONDS)
                .pollDelay(5, TimeUnit.SECONDS)
                .pollInterval(5, TimeUnit.SECONDS)
                .until(() -> isMailReceivedByReceiver(emailSubject, emailRecipient));
        log.info("Email with subject {} for recipient {} not found!", emailSubject, emailRecipient);
        return false;
    }

    /**
     * Check that email contains a specific text content in its body.
     *
     * @param expectedContent content text which we expect to be present in email body
     * @return true is desired content is present, otherwise false
     */
    public boolean emailHasCorrectContent(final String expectedContent) {
        try {
            final Address[] addresses = message.getRecipients(Message.RecipientType.TO);
            for (final Address address : addresses) {
                if (message.getSubject().contains(emailSubject) &&
                        address.toString().equals(emailRecipient)) {
                    return isContentPresentInEmail(getTextFromMessage(message), expectedContent);
                }
            }
        } catch (final MessagingException e) {
            throw new MobileAutomationException("Exception occurred when checking is email received!" + e.toString());
        }
        return false;
    }

    private void connectToGmailAndSearchForEmail(final String emailSubject) {
        try {
            final Folder mailFolder = connectToGmailInboxFolder();
            mailFolder.open(Folder.READ_WRITE);
            final SearchTerm searchTerm = new AndTerm(
                    new SubjectTerm(emailSubject),
                    new BodyTerm(emailSubject));
            messages = mailFolder.search(searchTerm);
        } catch (final MessagingException e) {
            log.info(e.toString());
        }
    }

    private boolean isContentPresentInEmail(final String emailContent, final String expectedContent) {
        if (emailContent.contains(expectedContent)) {
            log.info("{} is present in email content.", expectedContent);
            return true;
        } else {
            log.info("{} is not present in email content!", expectedContent);
            return false;
        }
    }

    private Folder connectToGmailInboxFolder() {
        final String host = "imap.gmail.com";
        final Properties props = new Properties();
        props.setProperty("mail.imap.ssl.enable", "true");
        final Session session = Session.getInstance(props);
        try {
            final Store store = session.getStore("imaps");
            store.connect(host, username, password);
            store.getDefaultFolder().list();
            return store.getFolder("INBOX");
        } catch (final MessagingException e) {
            throw new MobileAutomationException("Exception occurred when checking is email received!" + e.toString());
        }
    }

    private String getTextFromMessage(final Message message) {
        String result = "";
        try {
            if (message.isMimeType("text/plain")) {
                result = message.getContent().toString();
            } else if (message.isMimeType("multipart/*")) {
                final MimeMultipart mimeMultipart = (MimeMultipart) message.getContent();
                result = getTextFromMimeMultipart(mimeMultipart);
            }
        } catch (final MessagingException | IOException e) {
            throw new MobileAutomationException("Could not get text from email message!" + e.toString());
        }
        return result;
    }

    private String getTextFromMimeMultipart(final MimeMultipart mimeMultipart) {
        final StringBuilder result = new StringBuilder();
        try {
            final int count = mimeMultipart.getCount();
            for (int i = 0; i < count; i++) {
                final BodyPart bodyPart = mimeMultipart.getBodyPart(i);
                if (bodyPart.isMimeType("text/plain")) {
                    result.append("\n").append(bodyPart.getContent());
                    break;
                } else if (bodyPart.isMimeType("text/html")) {
                    final String html = (String) bodyPart.getContent();
                    result.append("\n").append(org.jsoup.Jsoup.parse(html).text());
                } else if (bodyPart.getContent() instanceof MimeMultipart) {
                    result.append(getTextFromMimeMultipart((MimeMultipart) bodyPart.getContent()));
                }
            }
        } catch (final MessagingException | IOException e) {
            throw new MobileAutomationException("Could not get text from Mime Multipart email message!" + e.toString());
        }
        return result.toString();
    }
}