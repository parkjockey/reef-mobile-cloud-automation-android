package us.reef.mobile.cloud.automation.storage;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

/**
 * @author Nikola Komazec (nikola.komazec@htecgroup.com)
 * Created on: 30/11/2020
 */
@Getter
@Setter
@Builder(toBuilder = true)
public class CreditCardEntity {
    private String cardNumber;
    private String cardName;
    private String cvv;
    private String month;
    private String year;
}
