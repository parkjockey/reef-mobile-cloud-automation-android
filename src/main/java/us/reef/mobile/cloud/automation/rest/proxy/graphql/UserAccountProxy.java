package us.reef.mobile.cloud.automation.rest.proxy.graphql;

import com.google.api.client.http.HttpStatusCodes;
import io.restassured.response.Response;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;
import us.reef.mobile.cloud.automation.exceptions.MobileAutomationException;
import us.reef.mobile.cloud.automation.rest.client.BaseRestClient;
import us.reef.mobile.cloud.automation.rest.data.graphql.responses.useraccount.UserAccountDetailsResponse;
import us.reef.mobile.cloud.automation.rest.data.graphql.variables.common.AppVersionVariables;
import us.reef.mobile.cloud.automation.utils.GraphQLUtil;

import java.io.File;

import static io.cucumber.spring.CucumberTestContext.SCOPE_CUCUMBER_GLUE;

/**
 * @author Nikola Komazec (nikola.komazec@htecgroup.com)
 * Created on: 05/11/2020
 */
@Slf4j
@Component
@Scope(SCOPE_CUCUMBER_GLUE)
public class UserAccountProxy {

    static File getUserAccountDetailsFile = new File("src/test/resources/graphql/account/getUserAccountDetails.graphql");
    @Value("${reef-mobile-graphql.url}")
    public String graphqlUrl;

    /**
     * Get User Account details.
     *
     * @return user account details {@link UserAccountDetailsResponse}
     */
    public UserAccountDetailsResponse getUserAccountDetails() {
        final BaseRestClient baseRestClient = new BaseRestClient(graphqlUrl);

        final Response response;
        final String graphqlPayload = GraphQLUtil.parseGraphql(getUserAccountDetailsFile, new AppVersionVariables());
        response = baseRestClient.post(graphqlPayload, "/graphql");
        if (response.getStatusCode() != HttpStatusCodes.STATUS_CODE_OK) {
            throw new MobileAutomationException("Get user account details response status code was {}, expected {}!",
                    response.getStatusCode(), HttpStatusCodes.STATUS_CODE_OK);
        }
        return response.as(UserAccountDetailsResponse.class);
    }
}