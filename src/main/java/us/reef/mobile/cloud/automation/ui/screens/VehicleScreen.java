package us.reef.mobile.cloud.automation.ui.screens;

import lombok.extern.slf4j.Slf4j;
import org.openqa.selenium.By;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;
import us.reef.mobile.cloud.automation.exceptions.MobileAutomationException;
import us.reef.mobile.cloud.automation.storage.VehicleEntity;
import us.reef.mobile.cloud.automation.ui.base.BaseDriver;
import us.reef.mobile.cloud.automation.ui.base.BasePage;

import java.util.ArrayList;
import java.util.List;

import static io.cucumber.spring.CucumberTestContext.SCOPE_CUCUMBER_GLUE;

/**
 * @author Nikola Komazec (nikola.komazec@htecgroup.com)
 * Created on: 06/11/2020
 */
@Slf4j
@Component
@Scope(SCOPE_CUCUMBER_GLUE)
public class VehicleScreen extends BasePage {

    final By vehicleScreenId = By.id("com.reeftechnology.reef.mobile:id/txt_title");
    final By carNickName = By.id("com.reeftechnology.reef.mobile:id/edit_car_nickname");
    final By licensePlateNumber = By.id("com.reeftechnology.reef.mobile:id/edit_licence_plate_number");
    final By licensePlateState = By.id("com.reeftechnology.reef.mobile:id/spinner_licence_plate_state");
    final By licensePlateStateList = By.id("com.reeftechnology.reef.mobile:id/select_dialog_listview");
    final By oversizedVehicle = By.id("com.reeftechnology.reef.mobile:id/cb_oversizedVehicle");
    final By saveButton = By.id("com.reeftechnology.reef.mobile:id/btn_save");

    public VehicleScreen(final BaseDriver baseDriver) {
        super(baseDriver);
    }

    /**
     * Check is Vehicle screen displayed.
     *
     * @return true if it is displayed, otherwise false.
     */
    public boolean isVehicleScreenDisplayed() {
        log.info("Checking is Vehicle screen displayed.");
        return waitForElementToBeDisplayed(vehicleScreenId);
    }

    /**
     * User fills in Vehicle form with data.
     *
     * @param carNickName        Car Nick Name.
     * @param licensePlateNumber License Plate Number.
     * @param licensePlateState  License Plate State.
     */
    public void fillInVehicleForm(final String carNickName,
                                  final String licensePlateNumber,
                                  final String licensePlateState) {
        waitAndSendKeys(this.carNickName, carNickName);
        waitAndSendKeys(this.licensePlateNumber, licensePlateNumber);
        waitAndTap(this.licensePlateState);
        waitForElementToBeDisplayed(this.licensePlateStateList);
        scrollAndClickOnElementByText(licensePlateState);
        log.info("Entered Car Nick name: {}, License Plate Number: {}, License Plate State: {}.",
                carNickName, licensePlateNumber, licensePlateState);
        hideKeyboard();
        final List<VehicleEntity> vehicle = new ArrayList<>();
        final VehicleEntity vehicleEntity = VehicleEntity.builder()
                .carNickName(carNickName)
                .licensePlateNumber(licensePlateNumber)
                .licensePlateState(licensePlateState)
                .build();
        vehicle.add(vehicleEntity);
        getStorage().setVehicles(vehicle);
        log.info("Vehicle added successfully.");
    }

    /**
     * Tap on Save button.
     */
    public void tapOnSaveButton() {
        if (driver.findElement(saveButton).getAttribute("enabled").equals("true")) {
            waitAndTap(saveButton);
            log.info("Tapped on Save button.");
        } else {
            throw new MobileAutomationException("Save button is not enabled!");
        }
    }
}