package us.reef.mobile.cloud.automation.rest.data.graphql.responses.common;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;

/**
 * @author Nikola Komazec (nikola.komazec@htecgroup.com)
 * Created on: 06/11/2020
 */
@Getter
@NoArgsConstructor
@AllArgsConstructor
public class Vehicle {
    private String carNickName;
    private String licensePlateNumber;
    private String normalizedLicensePlateNumber;
    private String licensePlateState;
    private Boolean isOversized;
    private Boolean isPrimary;
}
