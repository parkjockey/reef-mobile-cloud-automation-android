package us.reef.mobile.cloud.automation.ui.base;

import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.android.AndroidElement;
import lombok.Getter;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Scope;
import org.springframework.lang.Nullable;
import org.springframework.stereotype.Component;
import us.reef.mobile.cloud.automation.exceptions.MobileAutomationException;
import us.reef.mobile.cloud.automation.storage.Storage;

import java.io.File;
import java.net.MalformedURLException;
import java.net.URISyntaxException;
import java.net.URL;
import java.nio.file.Paths;
import java.util.Objects;

import static io.appium.java_client.remote.MobileCapabilityType.*;
import static io.cucumber.spring.CucumberTestContext.SCOPE_CUCUMBER_GLUE;

/**
 * @author Nikola Komazec (nikola.komazec@htecgroup.com)
 * Created on: 06/11/2020
 */
@Slf4j
@Component
@Setter
@Getter
@Scope(SCOPE_CUCUMBER_GLUE)
public class BaseDriver {

    private static final String PLATFORM = "Android";
    private final ThreadLocal<AndroidDriver<AndroidElement>> driver = new ThreadLocal<>();
    @Autowired
    private Storage storage;
    @Value("${bs.buildVersion}")
    private String buildVersion;
    @Value("${bs.appUrl}")
    private String appUrl;
    @Value("${bs.mobilePhone}")
    private String mobilePhone;
    @Value("${bs.osVersion}")
    private String osVersion;
    private boolean useRemoteWebDriver;
    private String remoteWebDriverUrl = null;
    private String localWebDriverUrl = null;
    private DesiredCapabilities capabilities = new DesiredCapabilities();

    /**
     * Setup method with selenium grid
     *
     * @param grid Selenium Grid, possible values "none", "local", "remote" (case insensitive)
     */
    public BaseDriver(@Value("${grid}") final String grid) {
        if (getGrid(grid).equals(Grid.NONE) || getGrid(grid).equals(Grid.LOCAL)) {
            this.useRemoteWebDriver = false;
            this.localWebDriverUrl = Grid.LOCAL.url;
        } else {
            this.useRemoteWebDriver = true;
            this.remoteWebDriverUrl = Grid.REMOTE.url;
        }
    }

    /**
     * Get driver
     *
     * @return {@link AndroidDriver}
     */
    public AndroidDriver<AndroidElement> getDriver() {
        return driver.get();
    }

    /**
     * Initialize WebDriver
     */
    public void initializeWebDriver() {
        if (this.useRemoteWebDriver) {
            setRemoteDesiredCapabilities();
            try {
                log.info("Initializing webDriver with url {} for platform {}.", remoteWebDriverUrl, PLATFORM);
                driver.set(new AndroidDriver<>(new URL(remoteWebDriverUrl), getCapabilities()));
            } catch (final MalformedURLException e) {
                final String msg = "Error while initializing remote WebDriver with url: " + remoteWebDriverUrl;
                log.error(msg, e);
                throw new MobileAutomationException(msg, e);
            }
        } else {
            log.info("Initializing driver.");
            setLocalDesiredCapabilities();
            try {
                driver.set(new AndroidDriver<>(new URL(localWebDriverUrl), getCapabilities()));
            } catch (final MalformedURLException e) {
                final String msg = "Error while initializing local WebDriver with url: " + localWebDriverUrl.toUpperCase();
                log.error(msg, e);
                throw new MobileAutomationException(msg, e);
            }
        }
    }

    /**
     * Get Application file
     *
     * @return application file {@link File}
     */
    private File getAppFile() {
        try {
            return Paths.get(Objects.requireNonNull(getClass()
                    .getClassLoader().getResource("app/app-uat-debug.apk.zip"))
                    .toURI()).toFile();
        } catch (final URISyntaxException e) {
            throw new MobileAutomationException("App path could not be found, please check! {}", e.toString());
        }
    }

    /**
     * Set Local Desired Capabilities
     */
    private void setLocalDesiredCapabilities() {
        capabilities.setCapability(PLATFORM_NAME, PLATFORM);
        capabilities.setCapability(PLATFORM_VERSION, osVersion);
        capabilities.setCapability(AUTOMATION_NAME, "UiAutomator2");
        capabilities.setCapability("autoGrantPermissions", "true");
        capabilities.setCapability("appPackage", "com.reeftechnology.reef.mobile");
        capabilities.setCapability("appActivity", "com.reeftechnology.reefmobile.presentation.splash.SplashActivity");
        capabilities.setCapability(APP, String.valueOf(getAppFile()));
    }

    /**
     * Set Remote Desired Capabilities
     */
    private void setRemoteDesiredCapabilities() {
        capabilities.setCapability("device", mobilePhone);
        capabilities.setCapability("os_version", osVersion);
        capabilities.setCapability("platformName", PLATFORM);
        capabilities.setCapability("browserstack.timezone", "Indianapolis");
        capabilities.setCapability("browserstack.appium_version", "1.18.0");
        capabilities.setCapability("browserstack.networkLogs", "true");
        capabilities.setCapability("browserstack.console", "verbose");
        capabilities.setCapability("realDevice", "true");
        capabilities.setCapability("autoGrantPermissions", "true");
        capabilities.setCapability("project", "Android - Reef Mobile Automation");
        capabilities.setCapability("build", buildVersion);
        capabilities.setCapability("name", storage.getScenario().getScenarioName());
        capabilities.setCapability("appActivity", "com.reeftechnology.reefmobile.presentation.splash.SplashActivity");
        capabilities.setCapability("appPackage", "com.reeftechnology.reef.mobile");
        capabilities.setCapability("app_url", appUrl);
        capabilities.setCapability("app", appUrl);
    }

    private Grid getGrid(@Nullable final String grid) {
        if (!StringUtils.isBlank(grid)) {
            return Grid.valueOf(grid.toUpperCase());
        } else {
            return Grid.NONE;
        }
    }

    /**
     * Tear down driver
     */
    public void tearDown() {
        log.info("Closing Driver for platform: {}.", PLATFORM);
        if (null != getDriver()) {
            getDriver().quit();
            driver.remove();
            log.info("Driver closed.");
        } else {
            throw new MobileAutomationException("In tearDown, driver is null!");
        }
    }
}