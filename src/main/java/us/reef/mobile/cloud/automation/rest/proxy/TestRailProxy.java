package us.reef.mobile.cloud.automation.rest.proxy;

import com.codepine.api.testrail.TestRail;
import com.codepine.api.testrail.model.Project;
import com.codepine.api.testrail.model.Result;
import com.codepine.api.testrail.model.Run;
import io.cucumber.java.Status;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;
import us.reef.mobile.cloud.automation.exceptions.MobileAutomationException;
import us.reef.mobile.cloud.automation.storage.Storage;

import java.util.ArrayList;
import java.util.List;

import static io.cucumber.spring.CucumberTestContext.SCOPE_CUCUMBER_GLUE;

/**
 * @author Nikola Komazec (nikola.komazec@htecgroup.com)
 * Created on: 06/11/2020
 */
@Slf4j
@Component
@Scope(SCOPE_CUCUMBER_GLUE)
public class TestRailProxy {

    @Autowired
    private Storage storage;

    @Value("${tr.username}")
    private String username;
    @Value("${tr.password}")
    private String password;
    @Value("${tr.endPoint}")
    private String endPoint;
    @Value("${tr.projectName}")
    private String projectName;
    @Value("${tr.testRun}")
    private String testRunName;

    private TestRail testRail;
    private Run run;

    /**
     * Start test run and set projectId for test run.
     */
    public void startTestRun() {
        if (testRail == null) {
            testRail = TestRail.builder(endPoint, username, password).build();
        }
        final List<Project> projects = testRail.projects().list().execute();
        final int projectId = projects.stream()
                .filter(p -> p.getName().equals(projectName))
                .findFirst()
                .orElseThrow(() -> new MobileAutomationException("Project id not found for Project {}.", projectName))
                .getId();
        run = getExistingTestRun(projectId);
    }

    private Run getExistingTestRun(final int projectId) {
        final List<Run> testRuns = testRail.runs().list(projectId).execute();
        if (testRuns.stream().anyMatch(r -> r.getName().equals(testRunName))) {
            return testRuns.stream().filter(r -> r.getName().equals(testRunName))
                    .findFirst().orElseThrow(() -> new MobileAutomationException("Test run id not found"));
        } else {
            throw new MobileAutomationException("Test run {} does not exist!", testRunName);
        }
    }

    /**
     * Set test case ids into storage.
     *
     * @param testCaseId test case id from scenario.
     */
    public void setTestCaseId(final String testCaseId) {
        final String cases = testCaseId.substring(testCaseId.indexOf("(") + 2, testCaseId.indexOf(")"))
                .replace("C", "");
        final String[] numbers = cases.split(",");
        final List<Integer> result = new ArrayList<>();
        for (final String number : numbers) {
            result.add(Integer.parseInt(number));
        }
        storage.getTestRail().setCaseIds(result);
    }

    /**
     * Set test case status in Test Rail.
     *
     * @param status status which will be set for test cases in Test Rail {@link Status}.
     */
    public void setTestCaseResultStatus(final Status status) {
        for (final Integer caseId : storage.getTestRail().getCaseIds()) {
            testRail.results().addForCase(run.getId(), caseId, new Result().setComment("Comment"),
                    testRail.resultFields().list().execute()).execute();
            final int statusId;
            switch (status) {
                case PASSED:
                    statusId = 1;
                    break;
                case FAILED:
                    statusId = 5;
                    break;
                case SKIPPED:
                    statusId = 2;
                    break;
                default:
                    throw new MobileAutomationException("Status not defined!");
            }
            testRail.results().addForCase(run.getId(), caseId, new Result().setStatusId(statusId),
                    testRail.resultFields().list().execute()).execute();
        }
    }
}