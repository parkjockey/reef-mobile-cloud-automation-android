package us.reef.mobile.cloud.automation.ui.base;

/**
 * @author Nikola Komazec (nikola.komazec@htecgroup.com)
 * Created on: 06/11/2020
 */
public enum Grid {

    // Local appium server
    NONE(""),
    LOCAL("http://localhost:4723/wd/hub"),
    REMOTE("https://reefglobal1:8Eq3XJAXxv9vJUsRKV4p@hub-cloud.browserstack.com/wd/hub");

    public final String url;

    Grid(final String gridUrl) {
        this.url = gridUrl;
    }
}