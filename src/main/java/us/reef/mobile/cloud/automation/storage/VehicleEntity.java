package us.reef.mobile.cloud.automation.storage;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

/**
 * @author Nikola Komazec (nikola.komazec@htecgroup.com)
 * Created on: 06/11/2020
 */
@Getter
@Setter
@Builder(toBuilder = true)
public class VehicleEntity {
    private String carNickName;
    private String licensePlateNumber;
    private String licensePlateState;
}
