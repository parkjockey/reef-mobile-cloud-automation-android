# REEF-Mobile-Cloud-Automation-Android

## Used Technologies

- [Java](https://www.java.com/)
- [JUnit 5](https://junit.org/junit5/)
- [AssertJ](http://joel-costigliola.github.io/assertj/)
- [Cucumber](https://docs.cucumber.io/)
- [Spring-context](https://mvnrepository.com/artifact/org.springframework/spring-context)
- [Project Lombok](https://projectlombok.org/)
- [REST-Assured](https://github.com/rest-assured/rest-assured/wiki/Usage)
- [Appium](https://github.com/appium/java-client)
- [Awaitility](https://github.com/awaitility/awaitility) 
- [Logback](https://logback.qos.ch/)
- [TestRail](https://github.com/codepine/testrail-api-java-client)

## Project usage

Project build executes by running command in project folder:
```console 
gradle clean build
```

Execution of tests (by default all scenarios with tag @ui):
```console
gradle cucumber
```

For example to execute @critical tests with 2 scenarios in parallel on Google Pixel 3 version 9.0:
```console 
gradle cucumber --tags="@critical" --threads="2" -Dbs.mobilePhone="Google Pixel 3" -Dbs.osVersion="9.0"
```

## Additional parameters:

#### Cucumber 
- `--tags` - scenario tags to execute i.e. ```--tags="@critical"```, if not specified `@ui` will be used.
- `--threads` - number of parallel scenarios i.e. ```--threads="2""```, if not specified `5` will be used

#### BrowserStack

- `-Dbs.mobilePhone` - mobile phone on which tests will be executed i.e. ```-Dbs.mobilePhone="Samsung Galaxy S9"```, if not specified `Google Pixel 3` will be used
- `-Dbs.osVersion` - os version on a mobile phone i.e. ```-Dbs.osVersion="8.0"```, if not specified `9.0` will be used
- `-Dbs.appUrl` - app url in BrowserStack i.e. ```-Dbs.appUrl="bs://123asd"```, if not specified appUrl from properties will be used

```
- Make sure mobilePhone and osVersion are compatible. (https://www.browserstack.com/app-automate/capabilities)
- App url can be uploaded using curl command when executing tests locally on BrowserStack.(https://www.browserstack.com/app-automate/rest-api)
- App url in BitRise is automatically uploaded and used in the build.
```

#### TestRail

- `-Dtr.executeRegression`- change status of test case in TestRail ```-Dtr.executeRegression="true"```, if not specified `false` will be used
- `-Dtr.projectName`- name of the project in TestRail i.e. ```-Dtr.projectName="MyProjectName""```, if not specified `Reef Mobile Touchless` will be used
- `-Dtr.testRun` - test run in TestRail i.e. ```-Dtr.testRun="MyTestRun"```, if not specified testRun from properties will be used

```
- projectName and testRun will only take effect when executeRegression is `true`
```

Feature files can be found on location ``src>test>resources>features``.

## Local Setup

### Download and install

- [Java 11](http://www.oracle.com/technetwork/java/javase/downloads/index.html)
- [Gradle 6.5.1+](https://gradle.org/install/)
- [Lombok](https://projectlombok.org/download)
- Cucumber plug-ins for chosen IDE, [IntelliJ IDEA Cucumber for Java plug-in](https://plugins.jetbrains.com/plugin/7212-cucumber-for-java) 
- Gherkin plugin for chosen IDE [IntelliJ IDEA](https://plugins.jetbrains.com/plugin/9164-gherkin)

### Running locally

- Make sure you have .apk file under app directory and its name set in BaseDriver class under getAppFile() method (do not push .apk to git)
- [Appium Desktop](https://github.com/appium/appium-desktop/releases/tag/v1.18.0-1) is installed and running at localhost:4723 (0.0.0.0:4723)
- Android SDK is installed on local machine
- Android Studio installed with android project and emulator is running
- In application.properties change `bs.mobilePhone` and `bs.osVersion` to simulator and `grid` to `local`

### Running on BrowserStack from local

- In application.properties make sure that `grid` is set to `remote`
- Make sure that `bs.appUrl` did not expire(BrowserStack removes it after 30days) [How to upload app to BrowserStack](https://www.browserstack.com/docs/app-automate/api-reference/appium/apps#upload-an-app)
- .apk file distributed from [Android project](https://bitbucket.org/parkjockey/reef-android-2.0/src/master/) 
- Set desired `bs.mobilePhone` and `bs.osVersion` [Device name and os version generator](https://www.browserstack.com/automate/capabilities)

### Inspect elements with Appium Desktop
- Start Inspector Session in Appium Desktop
- Remote Host is 127.0.0.1 with port 4723 (should be by default)
- Run Emulator from Android Studio
- For desired capabilities set 
```json
{
  "deviceName": "Android emulator (will find running emulator)",
  "platformName": "Android",
  "app": "pathToApkFile",
  "platformVersion": "11.0 (osVersion of the Emulator)",
  "appPackage": "com.reeftechnology.reef.mobile",
  "appActivity": "com.reeftechnology.reefmobile.presentation.splash.SplashActivity",
  "automationName": "Appium",
  "avd": "Pixel_XL_API_30 (name of Emulator from Android Studio, spaces are represented as '_')"
}
```
- Start the session - App should be installed on the Emulator and on Appium desktop you should see the started app with elements.

# Rules

### Features (Gherkin)

Features should be created under features' directory with a name and summary.

Each feature must contain at least one Scenario with description what is being tested.

Feature can contain Background section if those steps are not the focus of the scenario or steps which are repeating for other Scenarios.

We are using Gherkin syntax to write the scenarios:

```
Feature: Name of a feature

    This is a summary

    Background: I need to do something before scenario starts
        Given I need to have this
        And This as well
    
    Scenario: This is what I am testing
      Given Describe an initial context
      And Another initial context
      When Describe an event
      And Another event
      Then Describe an expected outcome
      And Another epected outcome
```

Given - It is typically something that happened in the past. 

When - Used to describe an event, or an action.

Then - Used to describe an expected outcome, or result.

And - Used for fluid structure.